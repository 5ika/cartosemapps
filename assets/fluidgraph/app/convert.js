/**
 * FluidGraph.prototype.d3DataToJsonD3 - Convert the current
 * graph d3Data into a JSON stringified format
 * @memberof FluidGraph.prototype
 * @return {string}  the D3 data in JSON format
 */
FluidGraph.prototype.d3DataToJsonD3 = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("d3DataToJsonD3 start");

  //We copy d3Data object, cause we don't want to modify it... serializing and deserializing...
  var localD3Data = JSON.parse(JSON.stringify(thisGraph.d3Data));

  localD3Data.edges.forEach(function(edge){
    edge.source = typeof edge.source.index != "undefined" ? edge.source.index : edge.source.id;
    edge.target = typeof edge.target.index != "undefined" ? edge.target.index : edge.target.id;
  });

  localD3Data.name = thisGraph.graphName;

  var jsonD3 = window.JSON.stringify(localD3Data);

  if (thisGraph.config.debug) console.log("d3DataToJsonD3 end");

  return jsonD3;
}

/**
 * FluidGraph.prototype.d3DataToJsonLd - Convert the current
 * graph d3Data into a JSON Linked Data object
 * @return {object}  the D3 data in JSON-LD
 */
FluidGraph.prototype.d3DataToJsonLd = function() {
  thisGraph = this;

/* Fonction à revoir suite aux changements dans CartoSemapps */

  if (thisGraph.config.debug) console.log("d3DataToJsonLd start");

  //We copy d3Data object, cause we don't want to modify it... serializing and deserializing...
  var localD3Data = JSON.parse(JSON.stringify(thisGraph.d3Data));

  localD3Data.nodes.forEach(function(node){
    node.index = node.index.toString();
    node.x = node.x.toString();
    node.y = node.y.toString();
  });

  if (localD3Data.edges.length)
  {
    localD3Data.edges.forEach(function(edge){
      edge.index = edge.index.toString();
      edge.source = edge.source["@id"];
      edge.target = edge.target["@id"];
    });
  }
  else localD3Data.edges = [];

  var jsonLd = {
                  "nodes": localD3Data.nodes,
                  "edges": localD3Data.edges,
                  "foaf:name": thisGraph.graphName
                };

  if (thisGraph.config.debug) console.log("d3DataToJsonLd end");

  return jsonLd;
}

/**
 * FluidGraph.prototype.jsonD3ToD3Data - Convert a JSON object
 * containing the graph information to a d3 formatted object
 *
 * @param  {object} jsonInput The json structure of the graph used as input data
 * @return {object}           Data that d3 will be able to process
 */
FluidGraph.prototype.jsonD3ToD3Data = function(jsonInput) {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("jsonGraphToData start");

  var d3data = {};
  var d3Nodes = [];
  var d3Edges = [];
  var typeOfjsonInput = typeof(jsonInput);
  if (typeOfjsonInput == "string")
  {
    var jsonObj = JSON.parse(jsonInput);
  }
  else if (typeOfjsonInput == "object"){
    var jsonObj = jsonInput;
  }
  thisGraph.graphName = jsonObj["foaf:name"];

  d3Nodes = jsonObj.nodes;
  d3Nodes.forEach(function(node){
    if (typeof node["@type"] == "undefined") return false;

    node.index = typeof node.index != "undefined" ? node.index : node.id ;
  });

  d3Edges = jsonObj.edges;
  d3Edges.forEach(function(edge){
    //Search node object corresponding to source
    var sourceFilter = d3Nodes.filter(function(node){
                            var booleanNode = node.index == edge.source || node.id == edge.source;
                            return booleanNode;
                          });

    //Search node object corresponding to target
    var targetFilter = d3Nodes.filter(function(node){
                            var booleanNode = node.index == edge.target || node.id == edge.target;
                            return booleanNode;
                          });
    if (typeof sourceFilter[0] == "object" && typeof targetFilter[0] == "object")
    {
      edge.source = sourceFilter[0];
      edge.target = targetFilter[0];
    }
    else return false;
  });

  if (thisGraph.config.debug) console.log("jsonGraphToData end");

  return {"nodes" : d3Nodes, "edges" : d3Edges};
}

FluidGraph.prototype.jsonLdToD3Data = function(jsonObj) {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("jsonLdToD3Data start");

  var d3Data = {};
  var d3Edges = [];
  var d3Nodes = [];

  thisGraph.graphName = jsonObj["foaf:name"];

  //Get the type of the object of nodes in case theres's only one node, it's an [object object]
  var typeObjNodes = Object.prototype.toString.call( jsonObj.nodes );
  if (typeObjNodes === '[object Array]')
    d3Nodes = jsonObj.nodes;
  else {
    d3Nodes.push(jsonObj.nodes);
  }

  d3Nodes.forEach(function(node){
    node.index = parseInt(node.index, 10);
    node.x = parseInt(node.x, 10);
    node.y = parseInt(node.y, 10);
  });

  if (jsonObj.edges)
  {
    //Get the type of the object of edges in case theres's only one edge, it's an [object object]
    var typeObjEdges = Object.prototype.toString.call( jsonObj.edges );
    if (typeObjEdges === '[object Array]')
      d3Edges = jsonObj.edges;
    else {
      d3Edges.push(jsonObj.edges);
    }

    d3Edges.forEach(function(edge){
      edge.source = d3Nodes.filter(function(node){
                              return node.index == edge.source || node["@id"] == edge.source;
                            })[0];

      edge.target = d3Nodes.filter(function(node){
                              return node.index == edge.target || node["@id"] == edge.target;
                            })[0];

      edge.index = parseInt(edge.index, 10);
    });
  }

  d3Data = {nodes : d3Nodes, edges : d3Edges};

  if (thisGraph.config.debug) console.log("jsonLdToD3Data end");

  return d3Data;
}

FluidGraph.prototype.jsonLdObjectToD3Data = function(jsonObj) {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("jsonLdObjectToD3Data start");

  var d3Data = {};
  var d3Nodes = [];
  var d3Edges = [];
  var objectsEdges = [];
  var type;
  var predicat;

  console.log("Début de la conversion des noeuds");

  // Get JSON/LD context
  thisGraph.jsonObjContext = jsonObj["@context"];

  /* ************************************
      Create d3Data.nodes
  ************************************* */
  var index = 0;
  jsonObj["@graph"].forEach(function(triple){
    var node;

    // if (triple.label && triple.label.indexOf("Assemblée Virt") > -1)
    //     console.log("log pour trouver la ligne de noeud");

    typeNodeObjectClass = thisGraph.getTypeFromTriple(triple);

    if (typeof typeNodeObjectClass != "undefined")
    {
      // On teste si ce type est dans les types attendus
      if (thisGraph.config.filterTypeNodeOption[typeNodeObjectClass])
      {
        node = thisGraph.prepareObjectD3Node(triple, typeNodeObjectClass, index);
        d3Nodes.push(node);
        index++;
        thisGraph.statisticsD3Data[typeNodeObjectClass] ++;
      }
    }
  });

  /* ************************************
      Prepare relations between nodes
  ************************************* */
  thisGraph.linkedByIndex = {};
  index = 0;
  console.log("Début de la récupération des liens, prédicats : ",thisGraph.config.filterTypeEdgeOption);
  jsonObj["@graph"].forEach(function(triple){
    var typeNodeObjectClass;
    var targetUriList = [];
    var uriFromContext;
    var typeObj;

   // if (triple["@id"].indexOf("food_france") > -1)
   //   console.log("log pour trouver la ligne de lien");

    typeNodeObjectClass = thisGraph.getTypeFromTriple(triple); // Exemple : "pair:hasInterest"

    if (typeof typeNodeObjectClass != "undefined")
    {
      // On parcours filterTypeEdgeOption pour tester tous les prédicats possibles
      for (var typeEdgeObjectClass in thisGraph.config.filterTypeEdgeOption)
      {
        //console.log(typeEdgeObjectClass);
        // On vérifie que ce type de lien est bien à "true"
        if (thisGraph.config.filterTypeEdgeOption[typeEdgeObjectClass])
        {
          // A triple can have multiple relations
          targetUriList = thisGraph.getTargetUriListFromTriple(triple, typeEdgeObjectClass);
          targetUriList.forEach(function(targetUri)
          {
            targetUri = targetUri.toLowerCase();

            var edge = thisGraph.prepareObjectD3Edge(triple, d3Nodes, typeEdgeObjectClass, targetUri, index);
            if (edge != "edgeError")
            {
//              if (edge.source.index == 153 || edge.target.index == 153)
//                console.log(edge);
              d3Edges.push(edge);
              // Création d'un tableau contenant tous les id des noeuds qui sont connectés ensemble
              // sous la forme "id1, id2 = type", ainsi, on peut retrouver facilement le type du lien plus tard
              thisGraph.linkedByIndex[edge.source.index + "," + edge.target.index] = edge["@type"];
              thisGraph.linkedByIndex[edge.target.index + "," + edge.source.index] = edge["@type"];
              index++;
              thisGraph.statisticsD3Data[typeEdgeObjectClass] ++;
            }
          });
        }
      }
    }
  });

  return { nodes : d3Nodes, edges : d3Edges}
}

FluidGraph.prototype.getTargetUriListFromTriple = function(triple, typeEdgeObjectClass) {
  var targetUriList = [];
  var typeObj;

  var typeEdgeObjectClassLabel = typeEdgeObjectClass.split(':').pop();

  typeObj = Object.prototype.toString.call( triple[typeEdgeObjectClassLabel] );
  if (typeObj === '[object Undefined]')
  { // test pair:hasInterest: ...
    typeObj = Object.prototype.toString.call( triple[typeEdgeObjectClass] );
    if (typeObj === '[object Undefined]')
    { // test http://virtual-assembly.org/ontologies/pair#hasInterest: ...
      if (typeof thisGraph.jsonObjContext[typeEdgeObjectClassLabel] != "undefined")
      {
        uriFromContext = thisGraph.jsonObjContext[typeEdgeObjectClassLabel]["@id"];
        typeObj = Object.prototype.toString.call( triple[uriFromContext] );
        if (typeObj === '[object Undefined]')
        { // Autre ?
          // console.log("Error "+typeEdgeObjectClass+" de type "+typeObj);
          // console.log(triple);
        }
        else { // http://virtual-assembly.org/ontologies/pair#hasInterest: ...
          if (typeObj === '[object Array]')
            targetUriList = triple[uriFromContext];
          else if (typeObj === '[object String]')
            targetUriList = triple[uriFromContext].split(',');
        }
      }
//      else break;
    }
    else { // pair:hasInterest: ...
      if (typeObj === '[object Array]')
        targetUriList = triple[typeEdgeObjectClass];
      else if (typeObj === '[object String]')
        targetUriList = triple[typeEdgeObjectClass].split(',');
    }
  }
  else { // hasInterest: ...
    if (typeObj === '[object Array]')
      targetUriList = triple[typeEdgeObjectClassLabel];
    else if (typeObj === '[object String]')
      targetUriList = triple[typeEdgeObjectClassLabel].split(',');
  }

  return targetUriList;
}

FluidGraph.prototype.prepareObjectD3Node = function(triple, typeNodeObjectClass, index, d3DataIsFiltered) {
  thisGraph = this;
  var d3Node = {};
  if (thisGraph.config.debug) console.log("prepareObjectD3Node start");

  d3Node["@id"] = triple["@id"];
  d3Node["@type"] = typeNodeObjectClass;
  d3Node["comment"] = triple["comment"];
  d3Node["description"] = triple["description"];
  d3Node["neighbours"] = [];
  d3Node.index = index;
  d3Node.i = index;

  if (typeNodeObjectClass == "pair:person")
  {
    if (triple.firstName || triple.lastName)
    {
      if (typeof triple["firstName"] == "undefined") triple["firstName"] = "";
      if (typeof triple["lastName"] == "undefined") triple["lastName"] = "";
      d3Node.label = triple["firstName"]+" "+triple["lastName"];
    }
    else if (triple.label)
    {
      d3Node.label = triple.label;
    }
    else {
      console.log("Pas de label pour ce type "+typeNodeObjectClass+" !");
      console.log(triple);
      return;
    }
  }
  else {
    if (typeof triple.label != "undefined")
      d3Node.label = triple["label"];
    else if (typeof triple["pair:label"] != "undefined")
      d3Node.label = triple["pair:label"];
    else{
      d3Node.label = "Erreur dans les datas !";
      console.log("Pas de label pour ce type "+typeNodeObjectClass+" !");
      console.log(triple);
    }
  }

  typeObjHomePage = Object.prototype.toString.call( triple["homePage"] );
  typeObjAboutPage = Object.prototype.toString.call( triple["aboutPage"] );
  if (typeNodeObjectClass == "pair:event")
  {
    if (typeObjAboutPage === '[object String]')
      d3Node["hypertext"] = triple["aboutPage"];
    else if (typeObjAboutPage === '[object Array]')
      d3Node["hypertext"] = triple["aboutPage"][0];
  }
  else
  {
    if (typeObjHomePage === '[object String]')
      d3Node["hypertext"] = triple["homePage"];
    else if (typeObjHomePage === '[object Array]')
      d3Node["hypertext"] = triple["homePage"][0];
  }

  if (thisGraph.config.displaySemappsLink && !d3Node["hypertext"])
  {
    // Objectif : obtenir une URL encodé (ou décodé) avec des %2F à la place des /
    // https://archipel.assemblee-virtuelle.org/Person/https%3A%2F%2Fdata.virtual-assembly.org%2Fusers%2Falice.poggioli/show
    // https://archipel.assemblee-virtuelle.org/person/https%3A%2F%2Fdata.virtual-assembly.org%2Fusers%2Falice.poggioli
    // https://archipel.assemblee-virtuelle.org/Person/https://data.virtual-assembly.org/users/alice.poggioli/show
    // triple["@id"] = https://data.virtual-assembly.org/users/alice.poggioli

    d3Node["hypertext"] = thisGraph.config.domainSemappsUri
                          +"/"+typeNodeObjectClass.split(":").pop() // /Person
                          +"/"+encodeURIComponent(triple["@id"])+"/show";
//    console.log(d3Node["hypertext"]);
  }

  if (thisGraph.config.debug) console.log("prepareObjectD3Node end");
  return d3Node;
}

FluidGraph.prototype.prepareObjectD3Edge = function(triple, d3Nodes, typeEdgeObjectClass, targetUri, index) {
  thisGraph = this;
  var d3Edge = {};
  var edgeError = false;
  if (thisGraph.config.debug) console.log("prepareObjectD3Edge start");

  d3Edge.index = index;
  d3Edge.i = index;
  d3Edge["@type"] = typeEdgeObjectClass;

  // retrouver l'@id (par exemple) "https://data.virtual-assembly.org/persons/michel_cadennes" dans tous les @id de d3Nodes
  d3Edge.source = d3Nodes.filter(function(node){
                          return node["@id"] == triple["@id"];
                        })[0];

  // et le relier à l'@id : "https://data.virtual-assembly.org/themes/_transition" dans tous les @id de d3Nodes
  d3Edge.target = d3Nodes.filter(function(node){
                          return node["@id"] == targetUri;
                        })[0];

  if (thisGraph.config.debug) console.log("prepareObjectD3Edge end");

  if (typeof d3Edge.source == "undefined")
  {
    // console.log(d3Edge);
    // d3Edge.source = d3Nodes[0];
    edgeError = true;
  }

  if (typeof d3Edge.target == "undefined")
  {
    // console.log(d3Edge);
    // d3Edge.target = d3Nodes[0];
    edgeError = true;
  }

  if (typeof d3Edge.source != "undefined" && typeof d3Edge.target != "undefined")
  {
//    if (d3Edge.source.index == 1741 || d3Edge.target.index == 1741)
//      console.log(d3Edge);

    // Exemple pour le noeud "FabMob", l'index est 681
    // Le lien est 23 -> 681
    // Il faut donc ajouter :
    // - 23 dans les voisins du noeud 681
    // - 681 dans les voisins du noeud 23
    // Attention : d3Edge.source correspond au noeud...
    // Si on ne trouve pas 23 dans les voisins du noeud target, on l'ajoute

    // Il arrive que des noeuds soient liés sur eux-mêmes
    if (d3Edge.source.index == d3Edge.target.index)
    {
      // console.log(d3Edge);
      // d3Edge.target = d3Nodes[0];
      edgeError = true;
    }

    if (!edgeError)
    {
      if (d3Edge.target.neighbours.indexOf(d3Edge.source.index) == -1)
        d3Edge.target.neighbours.push(d3Edge.source.index);
      // Si on ne trouve pas 681 dans le noeud source, on l'ajoute
      if (d3Edge.source.neighbours.indexOf(d3Edge.target.index) == -1)
        d3Edge.source.neighbours.push(d3Edge.target.index);
    }
  }

  if (thisGraph.config.debug) console.nodesEdgesOnlylog("prepareObjectD3Edge end");

  if (edgeError)
    return "edgeError";
  else
    return d3Edge;
}

FluidGraph.prototype.getTypeFromTriple = function(triple)
{
  // Récupération du type, qui peut être de multiple sortes
  // Soit une chaine de caractère avec une uri ou un foaf:person
  // Soit un objet contenant une uri et un foaf:person

  thisGraph = this;
  var type;
  var typeNodeObjectClass;

  var typeObj = Object.prototype.toString.call( triple["@type"] );
  if (typeObj === '[object Array]')
  {
    // On parcourt tous les types
    // http://www.w3.org/ns/ldp#BasicContainer
    // http://www.w3.org/ns/ldp#Container
    // http://www.w3.org/ns/activitystreams#Group
    // https://www.w3.org/ns/activitystreams#Person
    // http://virtual-assembly.org/ontologies/pair#Person
    // http://xmlns.com/foaf/0.1/Person
    // foaf:person
    triple["@type"].forEach(function(tripleType)
    {
      if (tripleType.indexOf("pair#") > -1)
      {
        type = tripleType.split("#").pop();
        typeNodeObjectClass = "pair:"+type.toLowerCase();
      }
      else if (tripleType.indexOf("foaf:") > -1)
      {
        type = tripleType.split(":").pop(); // foaf:person
        typeNodeObjectClass = "pair:"+type.toLowerCase();
      }
    });
  }
  else if (typeObj === '[object String]'){ //string
    if (triple["@type"].indexOf("pair#") > -1)
    {
      type = triple["@type"].split("#").pop();
      typeNodeObjectClass = "pair:"+type.toLowerCase();
    }
    else
    {
      type = triple["@type"].split("foaf:").pop(); // foaf:person
      typeNodeObjectClass = "pair:"+type.toLowerCase();
    }
  }
  else if (typeObj === 'Undefined'){
    if (thisGraph.config.debug) console.log("Prédicat @type undefined : "+triple["@type"]+" ("+triple["@id"]+")" );
    return;
  }
  else {
    if (thisGraph.config.debug) console.log("Prédicat @type bizarre : "+triple["@type"]+" ("+triple["@id"]+")" );
    return;
  }
  return typeNodeObjectClass;
}

FluidGraph.prototype.filterD3Data = async function(d3Data) {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("FilterD3Data start");

  var d3Nodes = [];
  var d3Edges = [];
  var objectsEdges = [];
  var type;
  var predicat;
  var nodeAlone;
  var typeNeighbour;

  thisGraph.statisticsD3DataFiltered = {
    "nodes" : 0,
    "edges" : 0,
// Classes
    "pair:person" : 0,
    "pair:organization" : 0,
    "pair:project" : 0,
    "pair:concept" : 0,
    "pair:resource" : 0,
    "pair:skill" : 0,
    "pair:theme" : 0,
    "pair:thema" : 0,
    "pair:organizationtype" : 0,
    "pair:event" : 0,
    "pair:place" : 0,

// Predicats
    "pair:hasInterest" : 0,
    "pair:involves" : 0,
    "pair:offeredBy" : 0,
    "pair:involvedIn" : 0,
    "pair:hasMember" : 0,
    "pair:offers" : 0,
    "pair:interestOf" : 0,
    "pair:memberOf" : 0,
    "pair:hostedIn" : 0,
    "pair:hasSubjectType" : 0,
    "pair:hasTopic" : 0,
    "pair:partnerOf" : 0,
    "pair:typeOfSubject" : 0,
    "pair:involves" : 0,
    "pair:membershipActor" : 0,
    "pair:supportBy" : 0,
    "pair:hasSkill" : 0,
    "pair:affiliatedBy" : 0,
    "pair:affiliates" : 0,
  };

  console.log("Début du filtrage des noeuds, classes : ",thisGraph.config.filterTypeNodeOption);

  /* ************************************
      Filter d3Data.nodes
  ************************************* */

  thisGraph.d3Data.nodes.forEach(function(d3Node)
  {
    // if (d3Node.label && d3Node.label.indexOf("Capitan") > -1)
    //    console.log("log pour trouver la ligne de noeud");

    // On ne conserve que les types actifs du filtre de class
    if (thisGraph.config.filterTypeNodeDisplay[d3Node["@type"]])
    {
      // On vérifie s'il a au moins un voisin non filtré ou si c'est un noeud seul
      nodeAlone = true;
      d3Node.neighbours.forEach(function(neighbourIndex)
      {
        typeNeighbour = thisGraph.getTypedNodeFromI(thisGraph.d3Data.nodes,neighbourIndex);
        if (typeNeighbour && thisGraph.config.filterTypeNodeDisplay[typeNeighbour])
        {
          nodeAlone = false;
        }
      });

      //Filter nodesEdgesOnly
      if (thisGraph.config.displayNodesAlone == "nodesEdgesOnly" && !nodeAlone
      || thisGraph.config.displayNodesAlone == "nodesOnly" && nodeAlone
      || thisGraph.config.displayNodesAlone == "all")
      {
        d3Nodes.push(d3Node);
        thisGraph.statisticsD3DataFiltered[d3Node["@type"]] ++;
      }
    }
  });

  /* ************************************
      Filter d3Data.edges
  ************************************* */
  console.log("Début du filtrage des liens, prédicats : ",thisGraph.config.filterTypeEdgeOption);
  thisGraph.d3Data.edges.forEach(function(d3Edge){
    // Exemple : d3Edge["@type"] = "involves"
    // On vérifie que ce type de lien est bien à "true"
    if (thisGraph.config.filterTypeEdgeOption[d3Edge["@type"]])
    {
      // On ne prend que les liens qui existent dans les Noeuds
      // d3Edge.source et d3edge.target sont des liens vers des noeuds

      //Search node object corresponding to source
      var sourceFilter = d3Nodes.filter(function(node){
                              var booleanNode = node["@id"] == d3Edge.source["@id"];
                              return booleanNode;
                            });

      //Search node object corresponding to target
      var targetFilter = d3Nodes.filter(function(node){
                              var booleanNode = node["@id"] == d3Edge.target["@id"];
                              return booleanNode;
                            });

      if (typeof sourceFilter[0] == "object" && typeof targetFilter[0] == "object")
      {
        d3Edges.push(d3Edge);
        thisGraph.statisticsD3DataFiltered[d3Edge["@type"]] ++;
      }
    }
  });

  return { nodes : d3Nodes, edges : d3Edges}

  if (thisGraph.config.debug) console.log("jsonLd ObjectToD3Data end");
}
