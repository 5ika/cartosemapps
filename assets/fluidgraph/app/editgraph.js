FluidGraph.prototype.createFluidGraph = function(externalGraph) {
    thisGraph = this;

    if (thisGraph.config.debug) console.log("createFluidGraph start");

  var name = externalGraph["foaf:name"];
  var firstName = externalGraph["foaf:firstName"];
  var currentProjectObject = externalGraph["foaf:currentProject"];
  var currentProjectId = currentProjectObject["@id"];
  thisGraph.d3Data.nodes = [{index : 0,
                              "@id" : externalGraph["@id"],
                              label : firstName+ " " +name,
                              "@type" : "av:actor"},
                              {index : 1,
                              "@id" : currentProjectId,
                              label : currentProjectId,
                              "@type" : "av:project"}
                            ];

  thisGraph.d3Data.edges = [{index:0,
                          "@id" : "http://fluidlog.com/edge/0",
                          "@type":"foaf:currentProject",
                          source: 0,
                          target: 1}
                          ];


  if (thisGraph.config.debug) console.log("createFluidGraph end");
}

FluidGraph.prototype.getOpenedGraph = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("rememberGraphOpened start");

  var openedGraphValue = localStorage.getItem(thisGraph.config.version+"|"+thisGraph.consts.OPENED_GRAPH_KEY);
  if (openedGraphValue)
  {
    thisGraph.config.typeServer = openedGraphValue.split("|",1)[0];
    openedGraph = openedGraphValue.split("|").pop();
  }
  else openedGraph = null;

  if (thisGraph.config.debug) console.log("rememberGraphOpened end");

  return openedGraph;
}

FluidGraph.prototype.setOpenedGraph = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("rememberGraphOpened start");

  var graphNameToSet;

  if (thisGraph.config.typeServer == "local")
    graphNameToSet = thisGraph.graphName;
  else { // external
    graphNameToSet = thisGraph.ldpGraphName;
  }

  localStorage.setItem(thisGraph.config.version
                      + "|" + thisGraph.consts.OPENED_GRAPH_KEY,
                      thisGraph.config.typeServer + "|" + graphNameToSet)

  if (thisGraph.config.debug) console.log("rememberGraphOpened end");
}

FluidGraph.prototype.newGraph = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("newGraph start");

  thisGraph.clearGraph();
  thisGraph.changeGraphName();
  thisGraph.config.typeServer = "local";

  thisGraph.d3Data.nodes = thisGraph.mockData0.nodes;
  localStorage.removeItem(thisGraph.config.version+"|"+thisGraph.consts.OPENED_GRAPH_KEY);
  thisGraph.initializeDisplay();
  thisGraph.initializeSimulation();
  thisGraph.movexy();

  var d3Node = thisGraph.getD3NodeFromIdentifier(thisGraph.mockData0.nodes[0]["@id"]);
  thisGraph.editNode.call(thisGraph, d3Node, thisGraph.mockData0.nodes[0]);
  //thisGraph.centerNode(thisGraph.mockData0.nodes[0]);

  if (thisGraph.config.debug) console.log("newGraph end");
}

FluidGraph.prototype.initDragLine = function(){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("initDragLine start");

  // line displayed when dragging new nodes
  if (thisGraph.config.curvesEdges)
  {
    thisGraph.drag_line = thisGraph.bgElement.append("path")
                          .attr("id", "drag_line")
                          .attr("class", "drag_line")
                          .attr("stroke-dasharray", "5,5")
                          .attr("stroke", "#999")
                          .attr("stroke-width", "2")
                          .attr("d", "M0 0 L0 0")
                          .attr("visibility", "hidden");
  }
  else {
    thisGraph.drag_line = thisGraph.bgElement.append("line")
                          .attr("id", "drag_line")
                          .attr("class", "drag_line")
                          .attr("stroke-dasharray", "5,5")
                          .attr("stroke", "#999")
                          .attr("stroke-width", "2")
                          .attr("x1", 0)
                    	    .attr("y1", 0)
                    	    .attr("x2", 0)
                    	    .attr("y2", 0)
                          .attr("visibility", "hidden");
  }

  if (thisGraph.config.debug) console.log("initDragLine start");
}

FluidGraph.prototype.changeGraphName = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("changeGraphName start");

  if (thisGraph.config.typeServer)
    $('#graphNameLabel').text(thisGraph.graphName + " ("+thisGraph.config.typeServer+")");
  else
    $('#graphNameLabel').text(thisGraph.graphName);

  if (thisGraph.config.debug) console.log("changeGraphName end");
}

FluidGraph.prototype.clearGraph = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("clearGraph start");

  thisGraph.resetMouseVars();
  thisGraph.resetStateNode();
  thisGraph.d3Data.nodes = [];
  thisGraph.d3Data.edges = [];
  thisGraph.d3DataFiltered.nodes = [];
  thisGraph.d3DataFiltered.edges = [];
  thisGraph.graphName = thisGraph.consts.UNTILTED_GRAPH_NAME;
  thisGraph.removeSvgElements();

  if (thisGraph.config.debug) console.log("clearGraph end");
}

FluidGraph.prototype.saveGraph = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("saveGraph start");

  if (thisGraph.config.typeServer == "local")
  {
    thisGraph.saveGraphToLocalStorage();
  }
  else { //external
    thisGraph.saveGraphToExternalStore();
  }

  if (thisGraph.config.debug) console.log("saveGraph end");

}

FluidGraph.prototype.saveGraphToLocalStorage = function() {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("saveGraphToLocalStorage start");

  thisGraph.changeGraphName();
  thisGraph.selectedGraphName = thisGraph.graphName;

  if (thisGraph.config.remindSelectedNodeOnSave == false)
  {
    thisGraph.d3Data.nodes.forEach(function(node, i){
      if (node.fixed == true) node.fixed = false;
    });
  }

  localStorage.setItem(thisGraph.config.version+"|"+thisGraph.graphName,
                      thisGraph.d3DataToJsonD3())

  $("#message").text("Locally saved!").show().delay(1000).fadeOut();

  thisGraph.setOpenedGraph();

  if (thisGraph.config.debug) console.log("saveGraphToLocalStorage end");
}

FluidGraph.prototype.saveGraphToExternalStore = function() {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("saveGraphToExternalStore start");

  var jsonLd = thisGraph.d3DataToJsonLd();

  if (thisGraph.graphName != thisGraph.consts.UNTILTED_GRAPH_NAME)
  {
    //add @id
    jsonLd["@id"] = thisGraph.ldpGraphName;

    //Bug : we do a get in order to get ETag
    // store.get(thisGraph.ldpGraphName)
    store.save(jsonLd);
    $("#message").text("Not saved, cause of bugs ! ;)").show().delay(1000).fadeOut();
  }
  else
  {
    store.save(jsonLd);
    $("#message").text("Saved in external store!").show().delay(1000).fadeOut();
  }

  // console.log("jsonLd " + JSON.stringify(jsonLd));

  if (thisGraph.config.debug) console.log("saveGraphToExternalStore end");
}

FluidGraph.prototype.publishGraph = function() {

  if (thisGraph.config.debug) console.log("publishGraph start");

  var jsonLd = thisGraph.d3DataToJsonLd();
  store.save(jsonLd);
  $("#message").text("Published in external store! You can open it ! :)").show().delay(1000).fadeOut();

  if (thisGraph.config.debug) console.log("publishGraph end");
}

FluidGraph.prototype.deleteExternalGraph = function(skipPrompt) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("deleteExternalGraph start");

  doDelete = true;
  if (!skipPrompt){
    doDelete = window.confirm("Press OK to delete the external graph named : " + thisGraph.graphToDeleteName);
  }
  if(doDelete){
    store.delete(thisGraph.graphToDeleteName);

    if (thisGraph.graphToDeleteName.split("/").pop() == thisGraph.graphName)
      thisGraph.newGraph();

    $("#closeManageGraphModal").click();

    thisGraph.graphToDeleteName = null;
  }

  if (thisGraph.config.debug) console.log("deleteExternalGraph end");
}

FluidGraph.prototype.deleteLocalGraph = function(skipPrompt) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("deleteLocalGraph start");

  doDelete = true;
  if (!skipPrompt){
    doDelete = window.confirm("Press OK to delete the local graph named : " + thisGraph.graphToDeleteName);
  }
  if(doDelete){
    localStorage.removeItem(thisGraph.config.version+"|"+thisGraph.graphToDeleteName);
    if (thisGraph.graphToDeleteName == thisGraph.graphName)
      thisGraph.newGraph();
  }

  if (thisGraph.config.debug) console.log("deleteLocalGraph end");
}

FluidGraph.prototype.drawNewLink = function (targetNode, sourceNode)
{
  thisGraph = this;

  if (thisGraph.config.debug) console.log("drawNewLink start");

  console.log("drawNewLink start (targetNode, sourceNode)", targetNode, sourceNode);

  thisGraph.OrderNodesInSvgSequence();

	//Don't create a link if there is already one
	var searchLinkSourceTarget = d3.select("#edge"+sourceNode.index+"_"+targetNode.index).node();
	var searchLinkTargetSource = d3.select("#edge"+targetNode.index+"_"+sourceNode.index).node();

	if (!searchLinkSourceTarget && !searchLinkTargetSource)
	{
		var newLink = thisGraph.diagonal()
				.source({"x":sourceNode.x, "y":sourceNode.y})
				.target({"x":targetNode.x, "y":targetNode.y})

    var existEdgeLabel = d3.select(".edgeLabel").node();
    var beforeElement;
    if (existEdgeLabel)
      beforeElement = ".edgeLabel";
    else {
      beforeElement = "#node";
    }

    var newPath = thisGraph.bgElement
				.insert("path", beforeElement)
        .attr("class", "link")
        .attr("id", "edge"+sourceNode.index + "_" + targetNode.index)
        .style("stroke", thisGraph.customEdges.strokeColor)
        .style("stroke-width", thisGraph.customEdges.strokeWidth)
				.style("fill", "none")
				.attr("d", newLink)

    thisGraph.svgEdgeLabel = thisGraph.bgElement.selectAll(".edgeLabel")
          .data(thisGraph.d3Data.edges)
          .enter()
          .insert("text", "#node")
          .attr("class", "edgeLabel")
          .attr("id", function(d) { return "label" + d.source.index + "_" + d.target.index})
      		.attr("x", function(d) { return d.source.x + (d.target.x - d.source.x)/2; })
          .attr("y", function(d) { return d.source.y + (d.target.y - d.source.y)/2; })
          .attr("text-anchor", "middle")
          .attr("visibility", "hidden")
          .attr("pointer-events", "none")
          .attr("cursor", "default")
      	  .style("fill", "#000")
          .text(function(d) {
            return d["@type"];
          });

		var totalLengthPath = newPath.node().getTotalLength();

		newPath
			.attr("stroke-dasharray", totalLengthPath + " " + totalLengthPath)
			.attr("stroke-dashoffset", totalLengthPath)
			.transition()
			.duration(thisGraph.customNodes.transitionDurationComeBack)
			.attr("stroke-dashoffset", 0)
			.on("end", function() {
				newPath.attr("stroke-dasharray", "none")

        thisGraph.svgEdges = thisGraph.bgElement.selectAll(".link")
                      			.data(thisGraph.d3Data.edges)

        thisGraph.svgEdges.on("mousedown", function(d){
                              thisGraph.linkOnMouseDown.call(thisGraph, d3.select(this), d);
                            })
                            .on("mouseup", function(d){
                              thisGraph.state.mouseDownLink = null;
                            })
                            .on("mouseover", function(d){
                              d3.select(this)
                              .transition()
                              .duration(thisGraph.customEdges.transitionDurationOverLinks)
                              .attr("stroke-width", thisGraph.customEdges.strokeWidth+10)
                              thisGraph.linkOnMouseDown.call(thisGraph, d3.select(this), d);
                            })
                            .on("mouseout", function(d){
                              d3.select(this)
                              .transition()
                              .duration(thisGraph.customEdges.transitionDurationOverLinks)
                              .attr("stroke-width", thisGraph.customEdges.strokeWidth)
                              thisGraph.state.mouseDownLink = null;
                              thisGraph.removeSelectFromLinks();
                            })
                            .on("dblclick", function(d){
                              thisGraph.linkEdit.call(thisGraph, d3.select(this), d);
                            })
      })
	}

  console.log("drawNewLink end d3Data = ", thisGraph.d3Data);

  if (thisGraph.config.debug) console.log("drawNewLink end");
}

FluidGraph.prototype.deleteLink = function() {
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("deleteLink start");

  if (thisGraph.d3Data.edges.length > 0)
  {
    thisGraph.d3Data.edges.splice(thisGraph.d3Data.edges.indexOf(thisGraph.state.selectedLink), 1);
    thisGraph.state.selectedLink = null;
    thisGraph.initializeDisplay();
    thisGraph.initializeSimulation();
  }
  else {
    console.log("No link to delete !");
  }

  if (thisGraph.config.debug) console.log("deleteLink end");
}

FluidGraph.prototype.deleteEdgesOfNodeFromD3Edges = function (nodeIdentifier, d3Edges) {
  var thisGraph = this;

  var toSplice = d3Edges.filter(
    function(l) {
      return (l.source["@id"] === nodeIdentifier) || (l.target["@id"] === nodeIdentifier);
    });

  toSplice.map(
    function(l) {
      d3Edges.splice(d3Edges.indexOf(l), 1);
    });

  return d3Edges;
}

FluidGraph.prototype.saveEditedLinkLabel = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("saveEditedLinkLabel start");

  var el = d3.select(thisGraph.state.editedLinkLabel);
  var p_el = d3.select(thisGraph.state.editedLinkLabel.parentNode);

  var linkLabelNewText;
  var textarea_edited_linklabel = p_el.select("#textarea_edited_linklabel");

  // thisGraph.state.editedLinkLabel = thisGraph.d3Data.edges[...]
  if (textarea_edited_linklabel.node().value == "")
    linkLabelNewText = thisGraph.customEdges.label.blankNodeLabel;
  else
    linkLabelNewText = textarea_edited_linklabel.node().value;

  thisGraph.state.editedLinkLabel.__data__["@type"] = linkLabelNewText;

  //Modification of linklabel
  var searchLabelIndex = "#label" + thisGraph.state.editedLinkLabel.__data__.source.index + "_" + thisGraph.state.editedLinkLabel.__data__.target.index;
  d3.select(searchLabelIndex).text(linkLabelNewText);

  d3.selectAll("#linkEditBox").remove();
  d3.selectAll("#fo_content_edited_linklabel").remove();

  thisGraph.state.editedLinkLabel = null;

  if (thisGraph.config.debug) console.log("saveEditNode end");
}

FluidGraph.prototype.updateSelectOpenGraphModalPreview = function(type) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("updateSelectOpenGraphModalPreview start");

  d3.select("#contentSelectedOpenGraphModalPreview").remove();

  var contentOpenGraphModalPreview = d3.select("#openSelectedGraphModalPreview")
              .append("div")
              .attr("id", "contentSelectedOpenGraphModalPreview")
  var ul =  contentOpenGraphModalPreview
                .append("ul")

  //Use every instead of forEach to stop loop when you want
  thisGraph.d3Data.nodes.every(function(node, index) {
    var li = ul
              .append("li")
              .text(node.label)
    if (index > 4)
      return false;
    else
      return true
  });

  var total = contentOpenGraphModalPreview
                .append("div")
                .attr("id","totalSelectedOpenGraphModalPreview")
                .html("<b>Total of nodes :</b> "+thisGraph.d3Data.nodes.length+"<br> <b>Total of links :</b> "+thisGraph.d3Data.edges.length);

  if (thisGraph.config.debug) console.log("updateSelectOpenGraphModalPreview end");
}

FluidGraph.prototype.selectOpenedGraphInModal = function(type) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayContentOpenGraphModal start");

  thisGraph.config.typeServer = type;

  if (thisGraph.config.typeServer == "local")
  {
    //Get index selected if selection change
    var openLocalGraphModalSelection = d3.select('#openLocalGraphModalSelection');
    if (openLocalGraphModalSelection.node())
    {
      var selectedoption = openLocalGraphModalSelection.node().selectedIndex;
      thisGraph.selectedGraphName = openLocalGraphModalSelection.node().options[selectedoption].value;
    }
    thisGraph.loadLocalGraph(thisGraph.selectedGraphName);
  }
  else if (thisGraph.config.typeServer == "external"){
    //Get index selected if selection change
    var openExternalGraphModalSelection = d3.select('#openExternalGraphModalSelection');
    if (openExternalGraphModalSelection.node())
    {
      var selectedoption = openExternalGraphModalSelection.node().selectedIndex;
      thisGraph.selectedLdpGraphName = openExternalGraphModalSelection.node().options[selectedoption].value;
    }
    thisGraph.loadLdpGraph(thisGraph.selectedLdpGraphName);
  }
  else if (!thisGraph.selectedGraphName)
  {
    thisGraph.selectedGraphName = thisGraph.graphName;
  }

  thisGraph.updateSelectOpenGraphModalPreview();

  if (thisGraph.config.debug) console.log("displayContentOpenGraphModal end");
}

FluidGraph.prototype.getListOfGraphsInLocalStorage = function(type) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getListOfGraphsInLocalStorage start");

  thisGraph.listOfLocalGraphs = [];
  Object.keys(localStorage)
      .forEach(function(key){
          var regexp = new RegExp(thisGraph.config.version);
           if (regexp.test(key)) {
             var keyvalue = [];
             keyvalue[0] = key.split("|").pop();
             keyvalue[1] = localStorage.getItem(key)

            if (keyvalue[0] != thisGraph.consts.OPENED_GRAPH_KEY)
             thisGraph.listOfLocalGraphs.push(keyvalue)
           }
       });

  if (type == "open")
  {
    if (thisGraph.listOfLocalGraphs.length)
    {
      d3.select('#openLocalGraphModalSelection').remove();
      openLocalGraphModalSelection = d3.select('#openLocalGraphModalList')
           .append("select")
           .attr("id", "openLocalGraphModalSelection")
           .attr("multiple", true)
           .attr("style","width:200px; height:100px")
           .on("click", function(d){
             thisGraph.selectOpenedGraphInModal.call(thisGraph, "local")})

      thisGraph.listOfLocalGraphs.forEach(function(value, index) {
       var option = openLocalGraphModalSelection
                   .append("option")
                   .attr("value", value[0])

                   if (thisGraph.graphName == thisGraph.consts.UNTILTED_GRAPH_NAME) //Untilted
                   {
                     if (index == 0)
                     {
                       option.attr("selected",true);
                       thisGraph.selectedGraphName = value[0];
                     }
                   }
                   else {
                     if (value[0] == thisGraph.selectedGraphName)
                     {
                       option.attr("selected",true);
                     }
                   }

       option.text(value[0])
      });
    }
  }

  if (thisGraph.config.debug) console.log("getListOfGraphsInLocalStorage end");
}

FluidGraph.prototype.getListOfGraphsInExternalStorage = function(externalStoreUri, type) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getListOfGraphsInExternalStorage start");

  thisGraph.listOfExternalGraphs = [];

  if (type == "open")
  {
    // Prepare list of external store(s) to open

    d3.select("#openExternalGraphModalSelection").remove();
    openExternalGraphModalSelection = d3.select("#openExternalGraphModalList")
          .append("select")
          .attr("id", "openExternalGraphModalSelection")
          .attr("multiple", true)
          .attr("style","width:200px; height:100px")
          .on("click", function(){
            thisGraph.selectOpenedGraphInModal.call(thisGraph,"external")
          });

  }
  else { // Manage

    // Prepare list of external store(s) to manage

    d3.select("#manageExternalGraphModalTable").remove();
    var manageExternalGraphModalTable = d3.select("#manageExternalGraphModalDivTable")
                              .append("table")
                              .attr("id", "manageExternalGraphModalTable")
                              .attr("class","ui celled table")

    var manageExternalGraphModalThead = manageExternalGraphModalTable
                              .append("thread")

    var manageExternalGraphModalTrHead =  manageExternalGraphModalTable
                            .append("tr")

    manageExternalGraphModalTrHead.append("th")
                              .text("Name")
    manageExternalGraphModalTrHead.append("th")
                              .text("Content")
    manageExternalGraphModalTrHead.append("th")
                              .text("Action")

    var manageExternalGraphModalTbody =  manageExternalGraphModalTable.append("tbody")
  }

  // Get data to build list

  var newstore = new MyStore({ container : externalStoreUri,
                            context : "http://owl.openinitiative.com/oicontext.jsonld",
                            template : "",
                            partials : ""})

  newstore.list(externalStoreUri).then(function(list){
    list.forEach(function(item){
      var idOfExternalGraph;
      var nameOfExternalGraph;
      newstore.get(item["@id"]).then(function(graphElementJsonld){
        idOfExternalGraph = graphElementJsonld["@id"].split("/").pop();
        nameOfExternalGraph = graphElementJsonld["foaf:name"];

        if (idOfExternalGraph)
        {
          if (type == "open")
          {
            var option = openExternalGraphModalSelection
                        .append("option")
                        .attr("value", thisGraph.externalStore.uri+idOfExternalGraph)

                        if (thisGraph.graphName == thisGraph.consts.UNTILTED_GRAPH_NAME) //Untilted
                        {
                          option.attr("selected",true);
                        }
                        else {
                          if (nameOfExternalGraph == thisGraph.selectedGraphName)
                          {
                            option.attr("selected",true);
                          }
                        }

            option.text(nameOfExternalGraph + " ("+idOfExternalGraph+")");
          }
          else { //manage

            var nodesPreview = "(";
            // When bug of list() will be resolved:
            // Change by graphElementJsonld.nodes and node.label...
            var dataNodes = graphElementJsonld["http://www.fluidlog.com/2013/05/loglink/core#node"];
            dataNodes.every(function(node, index){
              if (index > 2)
              {
                nodesPreview += node["rdfs:label"].split(" ",2);
                return false;
              }
              else {
                if (index == dataNodes.length-1)
                  nodesPreview += node["rdfs:label"].split(" ",2);
                else
                  nodesPreview += node["rdfs:label"].split(" ",2) + ',';
                return true;
              }
            });
            nodesPreview += ")";

            var manageExternalGraphModalTrBody =  manageExternalGraphModalTbody
                                    .append("tr")

            manageExternalGraphModalTrBody.append("td")
                                    .text(nameOfExternalGraph+ " ("+idOfExternalGraph+")");

            manageExternalGraphModalTrBody.append("td")
                                    .text(nodesPreview);

            manageExternalGraphModalTrBody.append("td")
                                  .append("button")
                                  .attr("class", "ui mini labeled icon button")
                                  .on("click", function (){
                                    thisGraph.graphToDeleteName = externalStoreUri+idOfExternalGraph;
                                    thisGraph.deleteExternalGraph.call(thisGraph);
                                    thisGraph.getListOfGraphsInLocalStorage.call(thisGraph, "manage");
                                  })
                                  .text("Delete")
                                  .append("i")
                                  .attr("class", "delete small icon")
          }
        }
      });
    });
  });

  if (thisGraph.config.debug) console.log("getListOfGraphsInExternalStorage end");
}

FluidGraph.prototype.displayContentManageGraphModal = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayContentManageGraphModal start");

  /*****************************
  *
  * Liste of local store(s)
  *
  ******************************/

  d3.select('#manageLocalGraphModalTable').remove();
  var manageLocalGraphModalTable = d3.select('#manageLocalGraphModalDivTable')
                            .append("table")
                            .attr("id", "manageLocalGraphModalTable")
                            .attr("class","ui celled table")

  var manageLocalGraphModalThead = manageLocalGraphModalTable
                            .append("thread")

  var manageLocalGraphModalTrHead =  manageLocalGraphModalTable
                          .append("tr")

  manageLocalGraphModalTrHead.append("th")
                            .text("Name")
  manageLocalGraphModalTrHead.append("th")
                            .text("Content")
  manageLocalGraphModalTrHead.append("th")
                            .text("Action")

  var manageLocalGraphModalTbody =  manageLocalGraphModalTable.append("tbody")


  thisGraph.listOfLocalGraphs.forEach(function(value, index) {
    try{
      var data = JSON.parse(value[1]);
    }catch(err){
      var data = null;
    }

    if (data)
    {
      var nodesPreview = "(";
      data.nodes.every(function(node, index){
        if (index > 2)
        {
          nodesPreview += node.label.split(" ",2);
          return false;
        }
        else {
          if (index == data.nodes.length-1)
            nodesPreview += node.label.split(" ",2);
          else
            nodesPreview += node.label.split(" ",2) + ',';
          return true;
        }
      });
      nodesPreview += ")";
    }

    var manageLocalGraphModalTrBody =  manageLocalGraphModalTbody
                            .append("tr")

    manageLocalGraphModalTrBody.append("td")
                            .text(value[0]);

    manageLocalGraphModalTrBody.append("td")
                            .text(' ' + nodesPreview);

    manageLocalGraphModalTrBody.append("td")
                          .append("button")
                          .attr("class", "ui mini labeled icon button")
                          .on("click", function (){
                            thisGraph.graphToDeleteName = value[0];
                            thisGraph.deleteLocalGraph.call(thisGraph);
                            thisGraph.config.typeServer = "local";
                            thisGraph.getListOfGraphsInLocalStorage.call(thisGraph, "manage");
                            thisGraph.displayContentManageGraphModal.call(thisGraph);
                          })
                          .text("Delete")
                          .append("i")
                          .attr("class", "delete small icon")
  });

  if (thisGraph.config.debug) console.log("displayContentManageGraphModal end");
}
