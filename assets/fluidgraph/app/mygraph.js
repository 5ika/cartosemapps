/* -------------------------------------------
//Exemple of code : https://bl.ocks.org/steveharoz/8c3e2524079a8c440df60c1ab72b5d03
- Récupération des donnée
- initializeAndDisplayGraph()
    -> initializeDisplay()
    -> initializeSimulation()
        -> initializeForces()
            -> updateForces()

----------------------------------------------*/

FluidGraph.prototype.initializeAndDisplayGraph = async function() {
  if (thisGraph.config.debug) console.log("initializeAndDisplayGraph start");

  $('#displayGraphLoaderId').addClass("active");
  $('#displayGraphButtonId').addClass("loading");

  await thisGraph.sleep(.1);

  console.log('Début de conversion des données dans d3DataFiltered + 1ms...');
  thisGraph.d3DataFiltered = await thisGraph.filterD3Data(thisGraph.d3Data);
  console.log('Fin de conversion des données dans d3DataFiltered + 1ms...');

  console.log("Début du rafraichissement des stats...");
  await thisGraph.updateStatistics(thisGraph.statisticsD3DataFiltered);
  console.log("Fin du rafraichissement des stats...");

  console.log('Début de préparation de la liste de recherche...');
  await thisGraph.initializeSearch();
  console.log('Fin de préparation de la liste de recherche...');

  console.log("Début de l'initialisation des noeuds et des liens...");
  await thisGraph.initializeNodesAndEdges();
  console.log("Fin de l'initialisation des noeuds et des liens...");

  console.log("Début de l'affichage du graph...");
  await thisGraph.displayGraph();
  console.log("Fin de l'affichage du graph...");

  $('#displayGraphButtonId').removeClass("loading");
  $('#displayGraphLoaderId').removeClass("active");

  if (thisGraph.config.debug) console.log("initializeAndDisplayGraph end");
}

FluidGraph.prototype.initializeSearch = async function() {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("initializeSearch start");

  thisGraph.searchData = [];
  thisGraph.d3DataFiltered.nodes.forEach(function(node) {
    thisGraph.searchData.push({title : node.label})
  });

  $('#searchId').show();
  $('#searchId').search({
      source: thisGraph.searchData,
      onSelect: function(result, response) {
        var dNode;
        d3Node = thisGraph.getD3NodeFromLabel(result.title);
        dNode = d3Node.node().__data__;
        thisGraph.centerNode(dNode);
        thisGraph.openNode(d3Node, dNode);
      },
      minCharacters: 0
  });

  if (thisGraph.config.debug) console.log("initializeSearch end");
}

// Initialise la taille des noeuds et l'épaisseur des liens en fonction du nombre d'objets à afficher
FluidGraph.prototype.initializeNodesAndEdges = async function() {
  console.log("initializeNodesAndEdges start");

  let dataToUse;

  if (thisGraph.d3DataFc)
    dataToUse = thisGraph.d3DataFc // Focus/context
  else if (thisGraph.d3DataFiltered.nodes.length > 0)
    dataToUse = thisGraph.d3DataFiltered // External Filtered (Semapps)
  else dataToUse = thisGraph.d3Data; // Local Fludy

  thisGraph.customEdges.proportionalStroke.forEach (element => {
    if (dataToUse.edges.length >= element.min && dataToUse.edges.length < element.max)
      thisGraph.customEdges.strokeWidth = element.value;
  });

  console.log("initializeNodesAndEdges end");
}

FluidGraph.prototype.displayGraph = async function() {
  if (thisGraph.config.debug) console.log("displayGraph start");

  thisGraph.resetMouseVars();
  thisGraph.removeSvgElements();

  await thisGraph.initializeDisplay();
  await thisGraph.initializeSimulation();
  await thisGraph.initializeForces();
  await thisGraph.updateForces();

  if (thisGraph.config.activeElasticity)  {
    thisGraph.simulation.on("tick", function(){
      thisGraph.movexy();
      d3.select('#alpha_value').style('flex-basis', (thisGraph.simulation.alpha()*100) + '%');
    });
  }  else { // Off
    for (var t = 100; t > 0; --t)
      thisGraph.simulation.tick();
    thisGraph.simulation.stop();
    thisGraph.movexy();
  }

  if (thisGraph.config.debug) console.log("displayGraph end");
}

FluidGraph.prototype.initializeFiltersInMenu = async function() {
  thisGraph = this;
  if (thisGraph.config.debug) console.log("loadFiltersInMenu start");

  $('#dropdown-filter-nodes').dropdown({
    'onLabelSelect': function (label) {
      console.log("onLabelSelect :"+label)
      if (label)
      {
        if (thisGraph.state.filterTypeMode != label.dataset.value)
        {
          thisGraph.state.filterTypeMode = label.dataset.value;
          thisGraph.MakeFilterNodesFirstInSvgSequence(label.dataset.value);
          thisGraph.highlightType(label.dataset.value, 0.3);
        }
        else if (thisGraph.state.filterTypeMode == label.dataset.value) {
          thisGraph.state.filterTypeMode = false;
          thisGraph.highlightType(label.dataset.value, 1);
        }
      }
    }
  });

  //Initialise dropdown filter for nodes
  for (let typeNodeOption in thisGraph.config.filterTypeNodeOption){
      optionLabel = typeNodeOption.split(":").pop();
      optionPairLabel = thisGraph.config.pairLabel[typeNodeOption];
      thisGraph.dataFilterTypeNodeOption.push({
        name : optionPairLabel,
        value : typeNodeOption,
        text : optionPairLabel+" ("+thisGraph.statisticsD3Data[typeNodeOption]+")",
        selected : thisGraph.config.filterTypeNodeDisplay[typeNodeOption]
      })
  };
  // Add filters
  $('#dropdown-filter-nodes').dropdown('change values',thisGraph.dataFilterTypeNodeOption);

  // Add or remove filters
  $('#dropdown-filter-nodes').dropdown('setting', {
    'onChange' : function(value, text, $choice) {
      console.log({value : value, text:text, choice:$choice})
      thisGraph.config.filterTypeNodeDisplay[value[0]] = true;
      if (thisGraph.config.colorizeFilterNode)
        $(".ui .label[data-value='"+value+"']")
        .prepend("<i class='circle icon' style='color:"+thisGraph.customNodes.colorType[value]+"'>");
    },
    'onRemove' : function(removedValue, removedText, $removedChoice){
      thisGraph.config.filterTypeNodeDisplay[removedValue] = false;
    }
  });

  // Customise color or click action for node filter
  if (thisGraph.config.colorizeFilterNode)
  {
    for (let typeNodeOption in thisGraph.config.filterTypeNodeOption){
      $(".ui .label[data-value='"+typeNodeOption+"']")
      .prepend("<i class='circle icon' style='color:"+thisGraph.customNodes.colorType[typeNodeOption]+"'>");
//      $(".ui .label[data-value='"+typeNodeOption+"']").click(alert(typeNodeOption))
    }
  }

  if (thisGraph.config.displayFilterEdge)
  {
    //Initialise dropdown filter for edges
    for (let typeEdgeOption in thisGraph.config.filterTypeEdgeOption){
      if (thisGraph.statisticsD3Data[typeEdgeOption] > 0)
      {
        optionLabel = typeEdgeOption.split(":").pop();
        optionPairLabel = thisGraph.config.pairLabel[typeEdgeOption];
        thisGraph.dataFilterTypeEdgeOption.push({
          name : optionPairLabel,
          value : typeEdgeOption,
          text : optionPairLabel+" ("+thisGraph.statisticsD3Data[typeEdgeOption]+")",
          selected : thisGraph.config.filterTypeEdgeOption[typeEdgeOption]
        })
      }
    };
    $('#dropdown-filter-edges').dropdown('change values',thisGraph.dataFilterTypeEdgeOption);

    $('#dropdown-filter-edges').dropdown('setting', {
      'onChange' : function(value, text, $choice) {
          thisGraph.config.filterTypeEdgeOption[value[0]] = true;
        },
      'onRemove' : function(removedValue, removedText, $removedChoice){
          thisGraph.config.filterTypeEdgeOption[removedValue] = false;
      }
    });
  }

  if (thisGraph.config.debug) console.log("loadFiltersInMenu end");
}

FluidGraph.prototype.initializeDisplay = async function(){
  if (thisGraph.config.debug) console.log("initializeDisplay start");

  let dataToUse;

  if (thisGraph.d3DataFc)
    dataToUse = thisGraph.d3DataFc // Focus/context
  else if (thisGraph.d3DataFiltered.nodes.length > 0)
    dataToUse = thisGraph.d3DataFiltered // External Filtered (Semapps)
  else dataToUse = thisGraph.d3Data; // Local Fludy

  if (typeof dataToUse.nodes != "undefined")
  {
    thisGraph.svgNodesEnter = thisGraph.bgElement
                                  .selectAll("#node")
    				                      .data(dataToUse.nodes); //, function(d) { return d.index;})

    thisGraph.svgNodes = thisGraph.svgNodesEnter
                                .enter()
                        				.append("g")
                                .attr("id", "node")
                                .attr("class", "g_node")
                                .attr("data-type", function(d) {
                                  return d["@type"];
                                })
                                .attr("index", function(d) { return d.index;})

                              if (thisGraph.config.allowDraggingNode == true)
                              {
                                thisGraph.svgNodes.call(d3.drag()
                                    .on("start", function(d){
                                      thisGraph.nodeOnDragStart.call(thisGraph, d3.select(this), d)
                                    })

                                    .on("drag", function(d){
                                      thisGraph.nodeOnDragMove.call(thisGraph, d3.select(this), d)
                                    })

                                    .on("end", function(d){
                                      thisGraph.nodeOnDragEnd.call(thisGraph, d3.select(this), d)
                                    })
                                )
                              }

    thisGraph.drawNodes(thisGraph.svgNodes);

    //delete node if there's less object in svgNodes array than in DOM
    thisGraph.svgNodesEnter.exit().remove();

    //Update links
    thisGraph.svgEdgesEnter = thisGraph.bgElement
                                        .selectAll(".link")
                  			                 .data(dataToUse.edges) //, function(d) { return d.source.index + "-" + d.target.index; })

    if (thisGraph.config.curvesEdges)
    {
      thisGraph.svgEdges = thisGraph.svgEdgesEnter
                          .enter()
                          .insert("path", "#node")
    }
    else
    {
      thisGraph.svgEdges = thisGraph.svgEdgesEnter
                          .enter()
                          .insert("line", "#node")
    }

      thisGraph.svgEdges.on("mousedown", function(d){
                          if (thisGraph.config.allowModifyLink == true)
                          {
                            thisGraph.linkOnMouseDown.call(thisGraph, d3.select(this), d);
                          }
                        })
                        .on("mouseup", function(d){
                          if (thisGraph.config.allowModifyLink == true)
                          {
                            thisGraph.state.mouseDownLink = null;
                          }
                        })
                        .on("mouseover", function(d){
                          d3.select(this)
                          .transition()
                          .duration(thisGraph.customEdges.transitionDurationOverLinks)
                          .attr("stroke-width", thisGraph.customEdges.strokeWidth+10)
                          thisGraph.linkOnMouseDown.call(thisGraph, d3.select(this), d);
                        })
                        .on("mouseout", function(d){
                            d3.select(this)
                            .transition()
                            .duration(thisGraph.customEdges.transitionDurationOverLinks)
                            .attr("stroke-width", thisGraph.customEdges.strokeWidth)
                            thisGraph.state.mouseDownLink = null;
                            thisGraph.removeSelectFromLinks();
                        })
                        .on("dblclick", function(d){
                          if (thisGraph.config.allowModifyLink == true)
                          {
                            thisGraph.linkEdit.call(thisGraph, d3.select(this), d);
                          }
                        })

    thisGraph.drawLinks(thisGraph.svgEdges);

    //delete link if there's less object in svgEdgesEnter array than in DOM
    thisGraph.svgEdgesEnter.exit().remove();

    if (thisGraph.customEdges.labelOnEdges)
    {
      thisGraph.svgEdgeLabelEnter = thisGraph.bgElement.selectAll(".edgeLabel")
          .data(dataToUse.edges, function(d) { return d.source.i + "-" + d.target.i; })

      thisGraph.svgEdgeLabel  =  thisGraph.svgEdgeLabelEnter
          .enter()
      		.insert("text", "#node")
          .attr("class", "edgeLabel")
          .attr("id", function(d) { return "label" + d.source.i + "_" + d.target.i })
          // Pas besoin de x,y, ils sont ajoutés automatiqument lors du movexy
          // Sinon, vu qu'on fait le movexy à la fin, il n'y a pas de coordonnées au moment du displayGraph
      		// .attr("x", function(d) {
          //   return d.source.x + (d.target.x - d.source.x)/2;
          // })
          // .attr("y", function(d) { return d.source.y + (d.target.y - d.source.y)/2; })
          .attr("text-anchor", "middle")
          .attr("visibility", "hidden")
          .attr("pointer-events", "none")
          .attr("cursor", "default")
      	  .style("fill", "#000")
          .text(function(d) {
            if (d["@type"])
              predicat = d["@type"];
            else
              predicat = "Erreur de prédicat";
            return predicat;
          });

      //delete label link if there's less object in svgedgeLabelEnter array than in DOM
      thisGraph.svgEdgeLabelEnter.exit().remove();
    }
  }

  if (thisGraph.config.debug) console.log("initializeDisplay end");
}

FluidGraph.prototype.initializeSimulation = async function(){
  let thisGraph = this;

  if (thisGraph.config.debug) console.log("initializeSimulation start");

  let dataToUse;

  if (thisGraph.d3DataFc)
    dataToUse = thisGraph.d3DataFc // Focus/context
  else if (thisGraph.d3DataFiltered.nodes.length > 0)
    dataToUse = thisGraph.d3DataFiltered // External Filtered (Semapps)
  else dataToUse = thisGraph.d3Data; // Local

  thisGraph.simulation = d3.forceSimulation();

  thisGraph.simulation.nodes(dataToUse.nodes);

  if (thisGraph.config.debug) console.log("initializeSimulation end");
}

FluidGraph.prototype.initializeForces = async function(){
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("initializeForces start");

  thisGraph.simulation.force( "link" , d3.forceLink() ) //.id(d => d.index) )
                      .force( "charge" , d3.forceManyBody() )
                      .force( "collide" , d3.forceCollide() )
                      .force( "center" , d3.forceCenter() )
                      .force( "forceX" , d3.forceX() )
                      .force( "forceY" , d3.forceY() );

  if (thisGraph.config.debug) console.log("initializeForces end");
}

FluidGraph.prototype.updateForces = function(){
  // Exemple : https://bl.ocks.org/steveharoz/8c3e2524079a8c440df60c1ab72b5d03
  // Doc : https://github.com/d3/d3-force

  let dataToUse;

  if (thisGraph.d3DataFc)
    dataToUse = thisGraph.d3DataFc // Focus/context
  else if (thisGraph.d3DataFiltered.nodes.length > 0)
    dataToUse = thisGraph.d3DataFiltered // External Filtered (Semapps)
  else dataToUse = thisGraph.d3Data; // Local

  thisGraph.simulation.force("center")
                      .x(thisGraph.svgContainer.width * thisGraph.config.force.center.x)
                      .y(thisGraph.svgContainer.height * thisGraph.config.force.center.y);

  thisGraph.simulation.force("charge")
                      .strength(thisGraph.config.force.charge.strength)
                      .theta(thisGraph.config.force.charge.theta)
                      .distanceMin(thisGraph.config.force.charge.distanceMin)
                      .distanceMax(thisGraph.config.force.charge.distanceMax);

  thisGraph.simulation.force("collide")
                      .radius(thisGraph.config.force.collide.radius)
                      .strength(thisGraph.config.force.collide.strength)
                      .iterations(thisGraph.config.force.collide.iterations);

  thisGraph.simulation.force("forceX")
                      .strength(thisGraph.config.force.forceX.strength * thisGraph.config.force.forceX.enabled)
                      .x(thisGraph.svgContainer.width * thisGraph.config.force.forceX.x);

  thisGraph.simulation.force("forceY")
                      .strength(thisGraph.config.force.forceY.strength * thisGraph.config.force.forceY.enabled)
                      .y(thisGraph.svgContainer.height * thisGraph.config.force.forceY.y);

  thisGraph.simulation.force("link")
//                      .id(d => d.label)
                      .distance(thisGraph.config.force.link.distance)
                      .strength(thisGraph.config.force.link.strength)
                      .iterations(thisGraph.config.force.link.iterations)
                      .links(dataToUse.edges);
}

FluidGraph.prototype.onInputChargeStrength = async function(value){
  d3.select('#chargeStrengthSliderOutputId').text(value);
  thisGraph.config.force.charge.strength = value;
  await thisGraph.updateForces();
  thisGraph.simulation.alpha(thisGraph.config.force.alphaTarget).restart();
  thisGraph.movexy();
}

FluidGraph.prototype.onInputCollideStrength = async function(value){
  d3.select('#collideStrengthSliderOutputId').text(value);
  thisGraph.config.force.collide.strength = value;
  await thisGraph.updateForces();
  thisGraph.simulation.alpha(thisGraph.config.force.alphaTarget).restart();
  thisGraph.movexy();
}

FluidGraph.prototype.onInputLinkDistance = async function(value){
  d3.select('#linkDistanceSliderOutputId').text(value);
  thisGraph.config.force.link.distance = value;
  await thisGraph.updateForces();
  thisGraph.simulation.alpha(thisGraph.config.force.alphaTarget).restart();
  thisGraph.movexy();
}

FluidGraph.prototype.onInputLinkStrength = async function(value){
  d3.select('#linkStrengthSliderOutputId').text(value);
  thisGraph.config.force.link.strength = value;
  await thisGraph.updateForces();
  thisGraph.simulation.alpha(thisGraph.config.force.alphaTarget).restart();
  thisGraph.movexy();
}

FluidGraph.prototype.movexy = async function(){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("movexy start");

  if (thisGraph.config.curvesEdges)
  {
    if (thisGraph.config.curvesEdgesType == "curve")
    {
      // curve
      thisGraph.bgElement.selectAll(".link").attr("d", function(d) {
          var dx = d.target.x - d.source.x,
              dy = d.target.y - d.source.y,
              dr = Math.sqrt(dx * dx + dy * dy);
         return "M" +
              d.source.x + "," +
              d.source.y + "A" +
              dr + "," + dr + " 0 0,1 " +
              d.target.x + "," +
              d.target.y;
      })
    }
    else { // diagonal
      // var link = thisGraph.bgElement.selectAll(".link"); // Deketed for V4
      // var node = thisGraph.bgElement.selectAll("#node");
      // var targetNode = thisGraph.state.targetNode;
      thisGraph.bgElement.selectAll(".link").attr("d", thisGraph.diagonal
      .x(function(d) {return d.x;})
      .y(function(d) {return d.y;}))
    }
  }
  else { // line
    thisGraph.bgElement.selectAll(".link").attr("x1", function(d) { return d.source.x; })
		      .attr("y1", function(d) { return d.source.y; })
		      .attr("x2", function(d) { return d.target.x; })
		      .attr("y2", function(d) { return d.target.y; });
  }

  thisGraph.bgElement.selectAll(".edgeLabel")
      .attr("x", function(d) {
        return d.source.x + (d.target.x - d.source.x)/2;
        })
      .attr("y", function(d) { return d.source.y + (d.target.y - d.source.y)/2; })

  thisGraph.bgElement.selectAll(".g_node")
  .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  // .attr("cx", function(d) { return d.x; })
  // .attr("cy", function(d) { return d.y; });

  if (thisGraph.config.debug) console.log("movexy end");
}

FluidGraph.prototype.sleep = function(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

FluidGraph.prototype.updateStatistics = async function(stats) {
  thisGraph = this;
  var typeNodeOptionLabel;
  var statsLabel;

  if (thisGraph.config.debug) console.log("updateStatistics start");

  if (thisGraph.d3Data.nodes)
  {
    thisGraph.statisticsD3Data.nodes = thisGraph.d3Data.nodes.length;
    thisGraph.statisticsD3Data.edges = thisGraph.d3Data.edges.length;
  }

  if (thisGraph.d3DataFiltered.nodes.length > 0)
  {
    thisGraph.statisticsD3DataFiltered.nodes = thisGraph.d3DataFiltered.nodes.length;
    thisGraph.statisticsD3DataFiltered.edges = thisGraph.d3DataFiltered.edges.length;
  }

  $('#statisticSegmentId').empty();
  $('#statisticSegmentId').append("<button class='ui black icon large button' id='refreshStatsSemappsButtonId'><i class='angle double right icon'></i></button>");

  $('#statisticSegmentId').append("<div class='ui mini inverted statistic' style='margin: 0px 2px 0px 2px;'><div class='value'>"
                        +stats["nodes"]
                        +"</div><div class='label'>Node</div></div>");

  for (var typeNodeOption in thisGraph.config.filterTypeNodeOption)
  {
    if(thisGraph.config.filterTypeNodeOption[typeNodeOption] && stats[typeNodeOption] > 0) // true or false ?
    {
      typeNodeOptionLabel = typeNodeOption.split(":").pop();
      statsLabel = thisGraph.config.pairLabel[typeNodeOption];
      $('#statisticSegmentId').append("<div class='ui mini "+thisGraph.config.statisticsColor[typeNodeOption]+" inverted statistic' style='margin: 0px 2px 0px 2px;'><div class='value'>"
                          +stats[typeNodeOption]
                          +"</div><div class='label'>"+statsLabel+"</div></div>");
    }
  }

  $('#statisticSegmentId').append("<div class='ui mini inverted statistic' style='margin: 0px 2px 0px 2px;'><div class='value'>"
                        +stats["edges"]
                        +"</div><div class='label'>Link</div></div>");
  $('#statisticSegmentId').append("<button class='ui black icon large button' id='refreshStatsCartoButtonId'><i class='reply icon'></i></button>");

  $('#refreshStatsSemappsButtonId').click(function() {
      thisGraph.updateStatistics(thisGraph.statisticsD3Data);
  });

  $('#refreshStatsCartoButtonId').click(function() {
      thisGraph.updateStatistics(thisGraph.statisticsD3DataFiltered);
  });

  if (thisGraph.config.debug) console.log("updateStatistics end");
}

FluidGraph.prototype.removeSvgElements = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("removeSvgElements start");

  d3.selectAll("#node").remove();
  d3.selectAll(".link").remove();
  d3.selectAll(".edgeLabel").remove();
  d3.selectAll(".templink").remove();
  d3.selectAll("#drag_line").remove();

  if (thisGraph.config.debug) console.log("removeSvgElements end");
}

// From : http://bl.ocks.org/robschmuecker/7880033, to be improoved... :)
FluidGraph.prototype.pan = function(domNode, direction) {

  if (thisGraph.config.debug) console.log("pan start");

/*
  var speed = thisGraph.panSpeed;
  var translateCoords, scaleX, scaleY, scale;
  var svgGroup = thisGraph.bgElement;

  if (thisGraph.panTimer) {
    clearTimeout(thisGraph.panTimer);
    translateCoords = d3.transform(svgGroup.attr("transform"));
    if (direction == 'left' || direction == 'right') {
      thisGraph.translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
      thisGraph.translateY = translateCoords.translate[1];
    } else if (direction == 'up' || direction == 'down') {
      thisGraph.translateX = translateCoords.translate[0];
      thisGraph.translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
    }
    scaleX = translateCoords.scale[0];
    scaleY = translateCoords.scale[1];
    scale = thisGraph.zoomListener.scale();
    svgGroup.transition().attr("transform", "translate(" + thisGraph.translateX + "," + thisGraph.translateY + ")scale(" + scale + ")");
    d3.select(domNode).select('g.node').attr("transform", "translate(" + thisGraph.translateX + "," + thisGraph.translateY + ")");
    thisGraph.zoomListener.scale(thisGraph.zoomListener.scale());
    thisGraph.zoomListener.translate([thisGraph.translateX, thisGraph.translateY]);
    thisGraph.panTimer = setTimeout(function() {
      thisGraph.pan(domNode, speed, direction);
    }, 50);
  }
*/
  if (thisGraph.config.debug) console.log("pan end");
}

FluidGraph.prototype.resetMouseVars = function()
{
  if (thisGraph.config.debug) console.log("resetMouseVars start");

  thisGraph.state.mouseDownNode = null;
  thisGraph.state.mouseUpNode = null;
  thisGraph.state.mouseDownLink = null;

  if (thisGraph.config.debug) console.log("resetMouseVars end");
}

FluidGraph.prototype.resetStateNode = function() {
  thisGraph.state.selectedNode = null;
  thisGraph.state.openedNode = null;
  thisGraph.state.editedNode = null;
  thisGraph.state.draggingNode = null;
  thisGraph.state.draggingNodeSvg = null;
  thisGraph.state.dragStarted = null;
  thisGraph.state.targetNode = null;
  thisGraph.state.targetNodeSvg = null;
}
